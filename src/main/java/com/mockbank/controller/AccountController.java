package com.mockbank.controller;

import com.mockbank.annotation.CustomLogger;
import com.mockbank.dto.AccountDTO;
import com.mockbank.dto.AccountResponseDTO;
import com.mockbank.service.AccountService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;

@RestController
public class AccountController {
    @Autowired
    AccountService accountService;

    @CustomLogger
    @PostMapping(value = "/account")
    public ResponseEntity<AccountResponseDTO> createAccount(@RequestBody AccountDTO account) {
        return new ResponseEntity<>(accountService.addAccount(account), HttpStatus.CREATED);
    }

    @CustomLogger
    @GetMapping("/accounts")
    public ResponseEntity<Page<AccountResponseDTO>> getAccounts(@RequestParam(defaultValue = "0", required = false) int pageNumber, @RequestParam(defaultValue = "10", required = false) int pageSize, @RequestParam(defaultValue = "accountNumber", required = false) String sortColumn, @RequestParam(defaultValue = "ASC", required = false) String order) {
        return new ResponseEntity<>(accountService.getAccounts(pageNumber, pageSize, sortColumn, order), HttpStatus.OK);
    }

    @CustomLogger
    @GetMapping("/account/{id}")
    public ResponseEntity<AccountResponseDTO> getAccountById(@PathVariable long id) {
        return new ResponseEntity<>(accountService.getById(id), HttpStatus.OK);
    }

    @CustomLogger
    @PutMapping(value = "/account/{id}")
    public ResponseEntity<AccountResponseDTO> updateAccountById(@RequestBody AccountDTO account, @PathVariable long id) {
        return new ResponseEntity<>(accountService.updateAccount(id, account), HttpStatus.OK);
    }

    @CustomLogger
    @DeleteMapping(value = "/account/{id}")
    public ResponseEntity<HttpStatus> deleteAccountById(@PathVariable long id) {
        return new ResponseEntity<>(accountService.deleteAccountById(id));
    }

}
