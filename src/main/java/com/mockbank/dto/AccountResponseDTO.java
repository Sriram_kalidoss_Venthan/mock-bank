package com.mockbank.dto;

import java.math.BigInteger;

public class AccountResponseDTO {

    private String firstName;
    private String lastName;
    private BigInteger accountNo;
    private AddressDTO addressDTO;

    public AccountResponseDTO() {
    }

    public AccountResponseDTO(String firstName, String lastName, BigInteger accountNo, AddressDTO addressDTO) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountNo = accountNo;
        this.addressDTO = addressDTO;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public BigInteger getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(BigInteger accountNo) {
        this.accountNo = accountNo;
    }

    public AddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(AddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }

    @Override
    public String toString() {
        return "AccountResponseDTO{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountNo=" + accountNo +
                ", addressDTO=" + addressDTO +
                '}';
    }
}
