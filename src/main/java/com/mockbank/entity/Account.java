package com.mockbank.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.OneToOne;
import javax.persistence.JoinColumn;
import javax.persistence.CascadeType;
import java.math.BigInteger;
import java.util.Random;

@Entity
@Table(name = "account")
public class Account {

    @Column(name = "firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    @Column(name = "accountNumber", nullable = false)
    private BigInteger accountNumber;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id", nullable = false)
    private Address address;


    public BigInteger accountNumberGenerator() {
        LocalDateTime myDateObj = LocalDateTime.now();
        String formattedAccountNumber = myDateObj.format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) + new Random().nextInt(3);
        return new BigInteger(formattedAccountNumber);
    }

    public Account() {
        this.accountNumber = this.accountNumberGenerator();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getId() {
        return id;
    }


    public BigInteger getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber() {
        this.accountNumber = this.accountNumberGenerator();
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

}
