package com.mockbank.service;

import com.mockbank.dto.AccountDTO;
import com.mockbank.dto.AccountResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


@Service
public interface AccountService {
    AccountResponseDTO addAccount(AccountDTO accountDTO);

    Page<AccountResponseDTO> getAccounts(int pageNumber, int pageSize, String sortColumn, String order);

    AccountResponseDTO getById(long id);

    AccountResponseDTO updateAccount(long id, AccountDTO acc);

    HttpStatus deleteAccountById(long id);
}
