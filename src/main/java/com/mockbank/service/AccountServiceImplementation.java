package com.mockbank.service;

import com.mockbank.dto.AccountDTO;
import com.mockbank.converters.AccountDtoConverter;
import com.mockbank.dto.AccountResponseDTO;
import com.mockbank.entity.Account;
import com.mockbank.exception.ResourceNotFoundException;
import com.mockbank.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service
public class AccountServiceImplementation implements AccountService {
    @Autowired
    AccountRepository accountRepository;

    @Override
    public AccountResponseDTO addAccount(AccountDTO accountDTO) {
        return AccountDtoConverter.toDTO(accountRepository.save(AccountDtoConverter.toAccount(accountDTO)));
    }

    @Override
    public Page<AccountResponseDTO> getAccounts(int pageNumber, int pageSize, String sortColumn, String order) {
        Page<Account> accountPages = accountRepository.findAll(PageRequest.of(pageNumber, pageSize, Sort.by(Sort.Direction.fromString(order.toUpperCase(Locale.ROOT)), sortColumn)));
        return accountPages.map(AccountDtoConverter::toDTO);
    }

    @Override
    public AccountResponseDTO getById(long id) {
        Account account = accountRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        return AccountDtoConverter.toDTO(account);
    }


    @Override
    public AccountResponseDTO updateAccount(long id, AccountDTO accountDTO) {
        Account accountToUpdate = accountRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        Account account = AccountDtoConverter.toAccount(accountToUpdate, accountDTO);
        accountRepository.save(account);
        return AccountDtoConverter.toDTO(account);
    }

    @Override
    public HttpStatus deleteAccountById(long id) {
        Account accountToDelete = accountRepository.findById(id).orElseThrow(ResourceNotFoundException::new);
        accountRepository.delete(accountToDelete);
        return HttpStatus.OK;
    }

}
